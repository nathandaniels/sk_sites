<?php
/*
  $Id: account_edit_process.php,v 1.2 2003/06/19 11:43:18 mdima Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Ihr Konto');
define('NAVBAR_TITLE_2', 'Ihre pers&ouml;nliche Daten &auml;ndern');
define('HEADING_TITLE', 'Ihre pers&ouml;nliche Daten &auml;ndern:');
?>
