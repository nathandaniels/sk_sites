<?php
/*
  $Id: account_edit_process.php,v 1.2 2003/06/19 11:43:17 mdima Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'My Account');
define('NAVBAR_TITLE_2', 'Edit Account');
define('HEADING_TITLE', 'My Account Information');
?>
