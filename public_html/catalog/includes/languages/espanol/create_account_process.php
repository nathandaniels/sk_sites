<?php
/*
  $Id: create_account_process.php,v 1.2 2003/06/19 11:43:17 mdima Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Crear una Cuenta');
define('NAVBAR_TITLE_2', 'Proceso');
define('HEADING_TITLE', 'Informacion de Mi Cuenta');

define('EMAIL_SUBJECT', 'Bienvenido a ' . STORE_NAME);
define('EMAIL_GREET_MR', 'Estimado ' . stripslashes($HTTP_POST_VARS['lastname']) . ',' . "\r\n\r\n");
define('EMAIL_GREET_MS', 'Estimado ' . stripslashes($HTTP_POST_VARS['lastname']) . ',' . "\r\n\r\n");
define('EMAIL_GREET_NONE', 'Estimado ' . stripslashes($HTTP_POST_VARS['firstname']) . ',' . "\r\n\r\n");
define('EMAIL_WELCOME', 'le damos la bienvenida a <b>' . STORE_NAME . '</b>.' . "\r\n\r\n");
define('EMAIL_TEXT', 'Ahora puede disfrutar de los <b>servicios</b> que le ofrecemos. Algunos de estos servicios son:' . "\r\n\r\n" . '<li><b>Carrito Permanente</b> - Cualquier producto a�adido a su carrito permanecera en el hasta que lo elimine, o hasta que realice la compra.' . "\r\n" . '<li><b>Libro de Direcciones</b> - Podemos enviar sus productos a otras direcciones aparte de la suya! Esto es perfecto para enviar regalos de cumplea�os directamente a la persona que cumple a�os.' . "\r\n" . '<li><b>Historia de Pedidos</b> - Vea la relacion de compras que ha realizado con nosotros.' . "\r\n" . '<li><b>Comentarios</b> - Comparta su opinion sobre los productos con otros clientes.' . "\r\n\r\n");
define('EMAIL_CONTACT', 'Para cualquier consulta sobre nuestros servicios, por favor escriba a: ' . STORE_OWNER_EMAIL_ADDRESS . '.' . "\r\n\r\n");
define('EMAIL_WARNING', '<b>Nota:</b> Esta direccion fue suministrada por uno de nuestros clientes. Si usted no se ha suscrito como socio, por favor comuniquelo a ' . STORE_OWNER_EMAIL_ADDRESS . '.' . "\r\n");
?>
