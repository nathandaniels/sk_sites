<?php
/*
  $Id: itransact.php,v 1.2 2003/06/19 11:43:14 mdima Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', '&Ouml;ffnen Sie die Transaction Systemsteuerung');
define('SUB_BAR_TITLE', 'Klicken Sie auf das Bild unten.  Sie ben&ouml;tigen Ihr Gateway ID und Passwort.');

define('CPANEL_URL', 'https://secure.itransact.com/support/login.html');
define('CPANEL_URL_NAME', 'ctrlpan');
?>
