<?php
/*
  $Id: itransact.php,v 1.2 2003/06/19 11:43:13 mdima Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Open Your Transaction Control Panel');
define('SUB_BAR_TITLE', 'Click the image below.  Your Gateway ID and Password are required.');

define('CPANEL_URL', 'https://secure.itransact.com/support/login.html');
define('CPANEL_URL_NAME', 'ctrlpan');
?>
